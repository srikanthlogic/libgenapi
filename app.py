import logging
from flask import Flask, request, jsonify
from libgen_api import LibgenSearch

app = Flask(__name__)

# Configure logging
logging.basicConfig(filename='api.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

@app.route('/', methods=['GET'])
def list_routes():
    """
    List all defined routes in the application.
    """
    try:
        output = []
        for rule in app.url_map.iter_rules():
            options = {}
            for arg in rule.arguments:
                options[arg] = "[{0}]".format(arg)

            route = {
                'endpoint': rule.endpoint,
                'methods': ', '.join(rule.methods),
                'path': rule.rule,
                'options': options,
            }
            output.append(route)

        return jsonify({'routes': output})
    except Exception as e:
        logging.error(f"Error in list_routes: {str(e)}")
        return jsonify({'error': 'Internal server error'}), 500

@app.route('/book', methods=['GET'])
def search_book():
    """
    Search for books by title.
    """
    try:
        # Get the query parameter 'title' from the request
        title = request.args.get('title')
        
        if not title:
            return jsonify({'error': 'Title query parameter is required'}), 400
        
        # Initialize the LibgenSearch instance
        libgen_search = LibgenSearch()
        
        # Search for the book by title
        results = libgen_search.search_title(title)
        
        return jsonify(results)
    except Exception as e:
        logging.error(f"Error in search_book: {str(e)}")
        return jsonify({'error': 'Internal server error'}), 500

@app.route('/author', methods=['GET'])
def search_author():
    """
    Search for books by author.
    """
    try:
        # Get the query parameter 'author' from the request
        author = request.args.get('author')
        
        if not author:
            return jsonify({'error': 'Author query parameter is required'}), 400
        
        # Initialize the LibgenSearch instance
        libgen_search = LibgenSearch()
        
        # Search for the book by author
        results = libgen_search.search_author(author)
        
        return jsonify(results)
    except Exception as e:
        logging.error(f"Error in search_author: {str(e)}")
        return jsonify({'error': 'Internal server error'}), 500

if __name__ == '__main__':
    app.run(debug=True)
