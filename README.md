# Libgen Search REST API

This is a simple Flask-based REST API for searching books and authors using the Library Genesis (Libgen) API. It allows you to query Libgen's database to find books by title or author.

## Getting Started

To run this API locally, follow these steps:

1. Clone the repository:

   ```bash
   git clone <repository_url>
   ```
2. Install the required dependencies:

    ```bash
    pip install -r requirements.txt
    ```

3. Start the Flask development server:
    ```bash 
    python app.py
    ```

The API should now be running at http://localhost:5000.

## Routes

### List Defined Routes

- **Route**: `/`
- **HTTP Method**: GET
- **Description**: List all defined routes in the application, including their paths, HTTP methods, and options.

### Search Books by Title

- **Route**: `/book`
- **HTTP Method**: GET
- **Parameters**:
  - `title` (query parameter): The title of the book to search for.
- **Description**: Search for books by title using the Libgen API.

### Search Books by Author

- **Route**: `/author`
- **HTTP Method**: GET
- **Parameters**:
  - `author` (query parameter): The name of the author to search for.
- **Description**: Search for books by author using the Libgen API.

## Example Usage

### List Defined Routes

To list all the defined routes in the application, make a GET request to the root route:

```bash
curl http://localhost:5000/
```


#### Search for Books by Title
```bash

curl http://localhost:5000/book?title=Pride%20and%20Prejudice
```
#### Search for Books by Author
```bash

curl http://localhost:5000/author?author=Jane%20Austen
```
### Error Handling
If a required query parameter is missing, the API will return a 400 Bad Request response with an error message. If an internal server error occurs, a 500 Internal Server Error response with an error message will be returned.

### Logging
The API logs errors and exceptions to the api.log file for troubleshooting and debugging purposes.

## License
This project is licensed under the MIT License - see the LICENSE file for details.

## Acknowledgments
The LibgenSearch library is used to interact with the [LibGen API](https://github.com/harrison-broadbent/libgen-api).
